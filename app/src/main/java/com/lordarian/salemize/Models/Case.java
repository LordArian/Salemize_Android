package com.lordarian.salemize.Models;

/**
 * Created by ebrah on 9/15/2017.
 */

public class Case {
    int id;
    String name;
    int price;
    String icon;
    int iconStatic;
    int type;
    String subtitle;

    public Case(int id, String name, int price, String icon, int iconStatic, int type, String subtitle) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.icon = icon;
        this.iconStatic = iconStatic;
        this.type = type;
        this.subtitle = subtitle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getIconStatic() {
        return iconStatic;
    }

    public void setIconStatic(int iconStatic) {
        this.iconStatic = iconStatic;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }
}
