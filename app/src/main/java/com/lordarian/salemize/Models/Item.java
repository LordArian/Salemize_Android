package com.lordarian.salemize.Models;

/**
 * Created by ebrah on 9/15/2017.
 */

public class Item {
    int id;
    String name;
    int type;
    int amount;
    int rarity;
    //

    public Item(int id, String name, int type, int amount, int rarity) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.amount = amount;
        this.rarity = rarity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
