package com.lordarian.salemize;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    Button profile;
    Button shop;
    Button guide;
    ImageView start;
    public static int RESULT_MAP = 1;
    public static int RESULT_PROFILE = 2;
    public static int RESULT_SHOP = 3;
    public static int RESULT_GUIDE = 4;
    public static int RESULT_SPIN = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FrameLayout f1 = (FrameLayout) findViewById(R.id.profile);
        profile = (Button) f1.findViewById(R.id.title_btn);
        profile.setText("پروفایل");
        FrameLayout f2 = (FrameLayout) findViewById(R.id.shop);
        shop = (Button) f2.findViewById(R.id.title_btn);
        shop.setText("فروشگاه");
        FrameLayout f3 = (FrameLayout) findViewById(R.id.guide);
        guide = (Button) f3.findViewById(R.id.title_btn);
        guide.setText("راهنما");
        start = (ImageView) findViewById(R.id.start_btn);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPage(MapsActivity.class, RESULT_MAP);
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openPage(ProfileActivity.class, RESULT_PROFILE);
            }
        });

        shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPage(ShopActivity.class, RESULT_SHOP);
            }
        });

        guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openPage(GuideActivity.class, RESULT_GUIDE);
            }
        });
    }

    public void openPage(Class activityClass, int requestCode){
        Intent intent = new Intent(this, activityClass);
        startActivityForResult(intent, requestCode);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

        }
    }
}
