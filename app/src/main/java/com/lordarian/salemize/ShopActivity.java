package com.lordarian.salemize;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.lordarian.salemize.Adapters.CaseAdapter;
import com.lordarian.salemize.Helpers.UI.FieldView;
import com.lordarian.salemize.Helpers.UI.UnitHelper;
import com.lordarian.salemize.Models.Case;

import java.util.ArrayList;

public class ShopActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    LinearLayout fieldsLayout;
    Context context;
    CaseAdapter addressAdapter;
    ArrayList<Case> cases;
    LinearLayoutManager layoutManager;
    ImageView backbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        context = this;
        uiInit();
    }

    private void uiInit() {
        Log.d("shopActivity", UnitHelper.pxToDp(context,30)+"x");
        layoutManager = new LinearLayoutManager(context);

        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(layoutManager);
        cases = new ArrayList<Case>();
        cases.add(new Case(1,"جعبه تصادفی",300,"X",R.mipmap.ic_question,0,""));
        cases.add(new Case(2,"جعبه اسنپ",700,"X",R.mipmap.ic_snapp,0,""));
        cases.add(new Case(3,"جعبه زودفود",1000,"X",R.mipmap.ic_snapp_food,0,""));
        cases.add(new Case(4,"جعبه دیجیکالا",1500,"X",R.mipmap.ic_digikala,0,""));
        cases.add(new Case(5,"جعبه پوشاک",3000,"X",R.mipmap.ic_clothing,0,""));
        cases.add(new Case(6,"جعبه شگفت انگیز",0,"X",R.mipmap.ic_chest,1,"(شامل ۳ آیتم شگفت انگیز)"));
        cases.add(new Case(7,"جعبه شگفت انگیز",0,"X",R.mipmap.ic_chest,1,"(شامل ۲ آیتم شگفت انگیز)"));
        cases.add(new Case(8,"جعبه شگفت انگیز",0,"X",R.mipmap.ic_chest,1,"(شامل ۴ آیتم شگفت انگیز)"));
        addressAdapter = new CaseAdapter(cases);
        recyclerView.setAdapter(addressAdapter);
        backbtn = (ImageView) findViewById(R.id.back_btn);
        fieldsLayout = (LinearLayout) findViewById(R.id.fields_layout);
        int imgs[] = {R.mipmap.ic_case,
                R.mipmap.ic_coins};
        for(int i=0;i<2;i++){
            FieldView field = new FieldView(this, i+"x", true, i == 0, imgs[i]);
            if(i == 0)
                field.setLayoutGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
            fieldsLayout.addView(field);
            field.setWeight(1);
        }
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
