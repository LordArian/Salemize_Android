package com.lordarian.salemize.Helpers.UI;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

/**
 * Created by ebrah on 9/16/2017.
 */

public class NSHorizontalScrollView extends HorizontalScrollView {
    private boolean enableScrolling = true;

    public boolean isEnableScrolling() {
        return enableScrolling;
    }

    public void setEnableScrolling(boolean enableScrolling) {
        this.enableScrolling = enableScrolling;
    }

    public NSHorizontalScrollView(Context context) {
        super(context);
    }

    public NSHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NSHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NSHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(isEnableScrolling()){
            return super.onInterceptTouchEvent(ev);
        }
        else{
            return false;
        }
    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if(isEnableScrolling()){
            return super.onTouchEvent(ev);
        }
        else{
            return false;
        }
    }
}
