package com.lordarian.salemize.Helpers.UI;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lordarian.salemize.Helpers.FormatHelper;
import com.lordarian.salemize.R;
import com.squareup.picasso.Picasso;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by ebrah on 9/15/2017.
 */

public class FieldView extends FrameLayout {

    Context context;
    ImageView icon;
    TextView title;
    View seperator;

    public FieldView(@NonNull Context context) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.field_view, this);
        icon = (ImageView) findViewById(R.id.field_icon);
        title = (TextView) findViewById(R.id.field_title);
        seperator = findViewById(R.id.field_seperator);
    }

    public FieldView(Context context, String title, float weight, boolean persianNum, boolean hideSeperator){
        this(context);
        setTitle(title,persianNum);
        setWeight(weight);
        if(hideSeperator)
            hideSeperator();
    }

    public FieldView(Context context, String title, boolean persianNum, boolean hideSeperator){
        this(context);
        setTitle(title,persianNum);
        if(hideSeperator)
            hideSeperator();
    }

    public FieldView(Context context, String title, boolean persianNum, boolean hideSeperator, int image){
        this(context,title,persianNum,hideSeperator);
        setIcon(image);
    }

    public void setIcon(int image){
        Picasso.with(context).load(image).into(this.icon);
    }

    private void hideSeperator() {
        this.seperator.setVisibility(GONE);
    }

    public void setTitle(String title){
        this.title.setText(title);
    }

    public void setTitle(String title, boolean persianNum){
        if(!persianNum)
            this.title.setText(title);
        else
            this.title.setText(FormatHelper.toPersianNumber(title));
    }

    public void setWeight(float weight){
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) this.getLayoutParams();
        params.weight = weight;
        this.setLayoutParams(params);
    }

    public void setLayoutGravity(int gravity){
        LayoutParams params = (LayoutParams) icon.getLayoutParams();
        params.gravity = gravity;

        params = (LayoutParams) title.getLayoutParams();
        params.gravity = gravity;

        params = (LayoutParams) seperator.getLayoutParams();
        params.gravity = gravity;

    }

}
