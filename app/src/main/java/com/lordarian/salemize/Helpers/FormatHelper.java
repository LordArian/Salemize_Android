package com.lordarian.salemize.Helpers;

/**
 * Created by ebrah on 9/14/2017.
 */

import android.text.TextUtils;
import android.util.Log;

public class FormatHelper {

    private static String[] persianNumbers = new String[]{ "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        String out = "";
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out += persianNumbers[number];
            } else if (c == '٫') {
                out += '،';
            } else {
                out += c;
            }
        }
        return out;
    }
    public static String toPersianNumber(String text, boolean separate){
        if(!separate)
            return toPersianNumber(text);
        int j=0;
        for(int i=text.length()-1;i>=0;i--){
            j++;
            if(j==3 && i>0) {
                text = text.substring(0, i) + "," + text.substring(i, text.length());
                j = 0;
            }
        }
        return toPersianNumber(text);
    }
}
