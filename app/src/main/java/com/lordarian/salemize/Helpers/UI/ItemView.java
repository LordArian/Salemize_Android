package com.lordarian.salemize.Helpers.UI;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lordarian.salemize.Helpers.FormatHelper;
import com.lordarian.salemize.R;
import com.squareup.picasso.Picasso;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by ebrah on 9/15/2017.
 */

public class ItemView extends FrameLayout {
    Context context;
    TextView title;
    TextView amount;
    View seperator;
    FrameLayout root;

    int weight;
    int rarc[] = {
            R.color.item_rarity1,
            R.color.item_rarity2,
            R.color.item_rarity3,
            R.color.item_rarity4
    };

    public ItemView(@NonNull Context context) {
        super(context);
        this.context = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_view, this);
        root = (FrameLayout) findViewById(R.id.root);
        title = (TextView) findViewById(R.id.item_title);
        amount = (TextView) findViewById(R.id.item_amount);
        seperator = findViewById(R.id.item_seperator);
    }

    public ItemView(Context context, int rarity, int type, int amount, int icon){
        this(context);
        Log.d("ITEM",rarity+ " x "+type+" x "+amount);
        if(type == 0){
            this.title.setText("کدتخفیف");
            this.amount.setText(FormatHelper.toPersianNumber(amount+"%"));
        }
        else if(type == 1){
            this.title.setText("اعتبار");
            this.amount.setText(FormatHelper.toPersianNumber(String.valueOf(amount),true));
        }
        this.amount.setTextColor(getResources().getColor(rarc[rarity-1]));
    }

    public void showItem(){
        this.root.animate().alpha(1).setDuration(800);
    }
}
