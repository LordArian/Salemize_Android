package com.lordarian.salemize.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lordarian.salemize.Helpers.FormatHelper;
import com.lordarian.salemize.Helpers.UI.UnitHelper;
import com.lordarian.salemize.Models.Case;
import com.lordarian.salemize.R;
import com.lordarian.salemize.SpinActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.lordarian.salemize.MainActivity.RESULT_SPIN;

/**
 * Created by ebrah on 9/15/2017.
 */

public class CaseAdapter extends RecyclerView.Adapter<CaseAdapter.holder> {

    private Context context;
    private static LayoutInflater inflater = null;
    ArrayList<Case> cases;

    public CaseAdapter(ArrayList<Case> cases){
        this.cases = cases;
    }

    @Override
    public CaseAdapter.holder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.case_view, parent, false);
        return new holder(inflatedView);
    }

    @Override
    public void onBindViewHolder(holder holder, int position) {
        final Case curCase = cases.get(position);
        holder.caseTitle.setText(curCase.getName());
        holder.caseSubtitle.setText(FormatHelper.toPersianNumber(curCase.getSubtitle()));
        holder.innerLayout.setPadding(UnitHelper.pxToDp(context,30),0,UnitHelper.pxToDp(context,30),0);
        if(curCase.getType() == 1){
            Picasso.with(context).load(curCase.getIconStatic()).into(holder.keyIcon);
            holder.keyPrice.setVisibility(View.VISIBLE);
            holder.casePrice.setVisibility(View.GONE);
            holder.coin.setVisibility(View.GONE);
            holder.caseIcon.setVisibility(View.GONE);
            if(position == cases.size() - 1){
                holder.seperator.setVisibility(View.GONE);
            }
            else{
                holder.seperator.setVisibility(View.VISIBLE);
            }
        }
        else{
            holder.seperator.setVisibility(View.VISIBLE);
            holder.caseIcon.setVisibility(View.VISIBLE);
            Picasso.with(context).load(R.mipmap.ic_box).into(holder.keyIcon);
            Picasso.with(context).load(curCase.getIconStatic()).into(holder.caseIcon);
            holder.keyPrice.setVisibility(View.GONE);
            holder.casePrice.setVisibility(View.VISIBLE);
            holder.coin.setVisibility(View.VISIBLE);
            holder.casePrice.setText(FormatHelper.toPersianNumber(String.valueOf(curCase.getPrice())));
            if(position == cases.size() - 2){
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)holder.seperator.getLayoutParams();
                params.setMargins(0,0,0,0);
            }
            else{
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)holder.seperator.getLayoutParams();
                params.setMargins(UnitHelper.dpToPx(context,30),0,UnitHelper.dpToPx(context,30),0);
            }

            if(curCase.getPrice()>900){
                holder.casePrice.setAlpha(0.5f);
                holder.coin.setAlpha(0.5f);
            }
            else{
                holder.casePrice.setAlpha(1f);
                holder.coin.setAlpha(1f);
            }

            if(position + 1 != cases.size()){
                if(cases.get(position + 1).getType() != curCase.getType()){
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)holder.seperator.getLayoutParams();
                    params.setMargins(0,0,0,0);
                }
                else{
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)holder.seperator.getLayoutParams();
                    params.setMargins(UnitHelper.dpToPx(context,30),0,UnitHelper.dpToPx(context,30),0);
                }
            }
            else{
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)holder.seperator.getLayoutParams();
                params.setMargins(UnitHelper.dpToPx(context,30),0,UnitHelper.dpToPx(context,30),0);
            }
        }
        holder.priceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(curCase.getPrice()<800 && curCase.getType() == 0) {
                    Intent intent = new Intent(context, SpinActivity.class);
                    intent.putExtra("name", curCase.getName());
                    intent.putExtra("icon", curCase.getIconStatic());
                    ((Activity) context).startActivityForResult(intent, RESULT_SPIN);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cases.size();
    }

    public class holder extends RecyclerView.ViewHolder{

        TextView keyPrice;
        TextView casePrice;
        ImageView coin;
        TextView caseTitle;
        ImageView caseIcon;
        ImageView keyIcon;
        View seperator;
        TextView caseSubtitle;
        FrameLayout innerLayout;
        FrameLayout priceLayout;

        public holder(View itemView) {
            super(itemView);
            keyPrice = (TextView) itemView.findViewById(R.id.key_price);
            caseTitle = (TextView) itemView.findViewById(R.id.case_title);
            caseIcon = (ImageView) itemView.findViewById(R.id.case_icon);
            keyIcon = (ImageView) itemView.findViewById(R.id.key_icon);
            seperator = itemView.findViewById(R.id.case_seperator);
            coin = (ImageView) itemView.findViewById(R.id.coin);
            casePrice = (TextView) itemView.findViewById(R.id.case_price);
            caseSubtitle = (TextView) itemView.findViewById(R.id.case_sub_title);
            innerLayout = (FrameLayout) itemView.findViewById(R.id.inner_layout);
            priceLayout = (FrameLayout) itemView.findViewById(R.id.price_layout);
        }
    }
}
