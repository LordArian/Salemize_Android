package com.lordarian.salemize;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lordarian.salemize.Helpers.FormatHelper;
import com.lordarian.salemize.Helpers.UI.BitmapHelper;
import com.lordarian.salemize.Helpers.Consts;
import com.lordarian.salemize.Helpers.LocationHelper;
import com.lordarian.salemize.Helpers.UI.FieldView;
import com.lordarian.salemize.Helpers.UI.UnitHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Vector;

import cz.msebera.android.httpclient.Header;
import info.hoang8f.android.segmented.SegmentedGroup;
import jp.wasabeef.blurry.Blurry;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    GoogleMap mGoogleMap;
    SupportMapFragment mFragment;
    Context context;
    LocationHelper locationHelper;
    public static final int REQUEST_LOCATION_PERMISSION = 1;

    private View mapView;
    private View locationButton;
    Vector<Marker> markers;
    LinearLayout fieldsLayout;
    TextView timeLeft;
    Button stopWalking;
    EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        context = this;
        uiInit();
    }

    private void uiInit() {
        markers = new Vector<>();
        timeLeft = (TextView) findViewById(R.id.time_left);
        fieldsLayout = (LinearLayout) findViewById(R.id.fields_layout);
        mFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        stopWalking = (Button) findViewById(R.id.stop_walking);

        mapView = mFragment.getView();
        requestPermissionLocation();
        mFragment.getMapAsync(this);

        int imgs[] = {R.mipmap.ic_distance,
                R.mipmap.ic_case,
                R.mipmap.ic_coins};
        for(int i=0;i<3;i++){
            FieldView field = new FieldView(this, i+"x", true, i == 0, imgs[i]);
            if(i == 0)
                field.setLayoutGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);
            fieldsLayout.addView(field);
            field.setWeight(1);
        }
        String ttl = "زمان تخمینی برای اتمام مسیر: ";
        String tl = ttl + FormatHelper.toPersianNumber("16 دقیقه");
        SpannableString ss =  new SpannableString(tl);
        ss.setSpan(new RelativeSizeSpan(0.88f), 0, ttl.length(), 0);
        ss.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.second_dark_gray)), 0, ttl.length(), 0);
        timeLeft.setText(ss);

        stopWalking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //BUILD CONFIG
        final FrameLayout config = (FrameLayout) findViewById(R.id.config_layout);
        ImageView closeBtn = (ImageView) findViewById(R.id.close_btn);
        Button startBtn = (Button) findViewById(R.id.start_btn);
        final TextView unit = (TextView) findViewById(R.id.unit);
        SegmentedGroup segments = (SegmentedGroup) findViewById(R.id.segments);
        input = (EditText) findViewById(R.id.input_field);

        final String units[] = {
                "(واحد:متر)",
                "(واحد:دقیقه)",
                "(واحد:کیلوکالری)"
        };
        String uttl = "میزان ";
        String utl = uttl + units[0];
        SpannableString uss =  new SpannableString(utl);
        uss.setSpan(new RelativeSizeSpan(1.5f), 0, uttl.length(), 0);
        unit.setText(uss);

        segments.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                Log.d("segment",checkedId+"x");
                int index = 0;
                if(checkedId == R.id.distance)
                    index = 0;
                else if(checkedId == R.id.time)
                    index = 1;
                else if(checkedId == R.id.calorie)
                    index = 2;
                String uttl = "میزان ";
                String utl = uttl + units[index];
                SpannableString uss =  new SpannableString(utl);
                uss.setSpan(new RelativeSizeSpan(1.5f), 0, uttl.length(), 0);
                unit.setText(uss);
            }
        });

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                config.setVisibility(View.GONE);
                getRoute(new LatLng(35.704272, 51.352161),new LatLng(35.699432, 51.338825));
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        Blurry.with(context)
//                .radius(5)
//                .sampling(2)
//                .async()
//                .onto((ViewGroup) findViewById(R.id.map_main));
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        mGoogleMap = gMap;
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(35.691578, 51.3306084), 12));
        mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
        buildMap();
    }

    public void buildMap(){
        try {
            boolean success = mGoogleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));
            if (!success) {
                Log.e("mapStyle", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("mapStyle", "Can't find style. Error: ", e);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(false);
        if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null){
            locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(UnitHelper.dpToPx(context, 40) , UnitHelper.dpToPx(context, 40));
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT , RelativeLayout.TRUE);
            layoutParams.setMargins(0 , 0 , UnitHelper.dpToPx(context, 20), UnitHelper.dpToPx(context, 10));
            locationButton.setLayoutParams(layoutParams);
        }

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        locationHelper = new LocationHelper();
        mGoogleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                if(!LocationHelper.isLocationEnabled(context)){
                    Toast.makeText(context, "برای ردیابی محل خود لطفا جی پی اس گوشی خود را روشن کنید.", Toast.LENGTH_LONG).show();
                    locationHelper.requestForLocationOn(context);
                }
                return false;
            }
        });

    }

    private void getRoute(LatLng origin, LatLng destination) {
        markers.clear();
        BitmapDescriptor bitmap1 = null;
        BitmapDescriptor bitmap2 = null;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            bitmap1 = BitmapDescriptorFactory.fromBitmap(BitmapHelper.getBitmapFromVectorDrawable(context,R.drawable.marker_me));
            bitmap2 = BitmapDescriptorFactory.fromBitmap(BitmapHelper.getBitmapFromVectorDrawable(context,R.drawable.marker_end));
        } else{
            bitmap1 = BitmapDescriptorFactory.fromResource(R.drawable.marker_me);
            bitmap2 = BitmapDescriptorFactory.fromResource(R.drawable.marker_end);
        }
        ApplicationInfo app = null;
        try {
            app = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Bundle bundle = app.metaData;
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.add("origin",origin.latitude+","+origin.longitude);
        params.add("destination",destination.latitude+","+destination.longitude);
        params.add("sensor","false");
        params.add("units","metrics");
        params.add("sensor","walking");
        params.add("key",bundle.getString("com.google.android.geo.API_KEY"));
        final BitmapDescriptor finalBitmap = bitmap2;
        final BitmapDescriptor finalBitmap1 = bitmap1;
        client.get(context, Consts.googleDirectionApiUrl, params, new JsonHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("routes");
                    JSONObject routes = jsonArray.getJSONObject(0);
                    JSONObject polyLinePoints = routes.getJSONObject("overview_polyline");

                    String polyLineStringEncoded = polyLinePoints.getString("points");

                    if(!polyLineStringEncoded.isEmpty()){
                        String.valueOf(polyLineStringEncoded.replaceAll("\n" , ""));
                    }

                    List<LatLng> polylines = PolyUtil.decode(polyLineStringEncoded);
                    PolylineOptions opts = new PolylineOptions()
                            .color(context.getResources().getColor(R.color.line));

                    int i = 0;
                    for(LatLng latlng: polylines){
                        opts.add(latlng);
                        Marker marker = null;
                        if(i > 0 && i < polylines.size() - 1) {
                            marker = mGoogleMap.addMarker(new MarkerOptions().position(latlng));
                            marker.setVisible(false);
                        }
                        else{
                            marker = mGoogleMap.addMarker(new MarkerOptions()
                                    .position(latlng)
                                    .icon(i == 0 ? finalBitmap1 : finalBitmap));
                            marker.setAnchor(0.5f, 0.5f);

                        }
                        markers.add(marker);
                        i++;
                    }
                    Polyline line = mGoogleMap.addPolyline(opts);
                    line.setWidth(7f);
                    changeCamera();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.d("getDirection","Failure "+errorResponse);
            }
        });
    }

    private void changeCamera() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        if(markers.size() == 0){
            Log.d("markers","empty");
            return;
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate cu;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            final int width = getResources().getDisplayMetrics().widthPixels;
            final int height = getResources().getDisplayMetrics().heightPixels;
            cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, UnitHelper.dpToPx(context,40));
            mGoogleMap.animateCamera(cu);
        } else {
        }
    }

    public void requestPermissionLocation() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                Log.d("REQ","umad");
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    buildMap();

                } else {

                    Toast.makeText(this, "برای استفاده از اپلیکیشن لطفا اجازه دسترسی به محل را تایید کنید.", Toast.LENGTH_SHORT).show();
                    requestPermissionLocation();
                }
                return;
            }
        }
    }

    private static long calculateDistance(LatLng prev, LatLng now) {
        double lat1 = prev.latitude;
        double lng1 = prev.longitude;
        double lat2 = now.latitude;
        double lng2 = now.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        long distanceInMeters = Math.round(6371000 * c);
        return distanceInMeters;
    }

    public void showSpeed(){

    }

    @Override
    public void onLocationChanged(Location location) {

    }

}