package com.lordarian.salemize;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lordarian.salemize.Helpers.UI.ItemView;
import com.lordarian.salemize.Helpers.UI.NSHorizontalScrollView;
import com.lordarian.salemize.Helpers.UI.UnitHelper;
import com.squareup.picasso.Picasso;

import java.util.Random;

public class SpinActivity extends AppCompatActivity {

    LinearLayout items;
    Context context;
    NSHorizontalScrollView hscroll;
    int targetPosition = 23;
    Button startSpinBtn;
    TextView titleView;
    String title;
    int icon;
    FrameLayout caseIconLayout;
    ImageView caseIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spin);
        context = this;
        title = getIntent().getStringExtra("name");
        icon = getIntent().getIntExtra("icon",-1);
        titleView = (TextView) findViewById(R.id.title);
        items = (LinearLayout) findViewById(R.id.item_list);
        hscroll = (NSHorizontalScrollView) findViewById(R.id.hscrollview);
        hscroll.setEnableScrolling(false);
        startSpinBtn = (Button) findViewById(R.id.start_spin);
        caseIcon = (ImageView) findViewById(R.id.case_icon);
        caseIconLayout = (FrameLayout) findViewById(R.id.case_icon_layout);

        titleView.setText(title);
        Picasso.with(context).load(icon).into(caseIcon);

        final Random r = new Random();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 40; i++) {
                    int rar = r.nextInt(4) + 1;
                    int typ = r.nextInt(2);
                    int amo = 0;
                    int begin = 0, end = 0;
                    if(typ == 0){
                        amo = r.nextInt(10) + rar * 10;
                    }
                    else{
                        amo = (r.nextInt(rar) + 1) * 5000;
                    }
                    ItemView item = new ItemView(context,rar,typ,amo,icon);
                    items.addView(item);
                }
            }
        },0);
        startSpinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<items.getChildCount();i++){
                    ItemView item = (ItemView) items.getChildAt(i);
                    item.showItem();
                }
                caseIconLayout.animate().alpha(0).setDuration(800);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollToTarget();
                        startSpinBtn.setVisibility(View.GONE);
                    }
                },800);
            }
        });
    }

    private void scrollToTarget() {
        ItemView targetItem = (ItemView) items.getChildAt(targetPosition);
        ObjectAnimator.ofInt(hscroll, "scrollX",  targetItem.getLeft() + UnitHelper.dpToPx(context, 60)).setDuration(3500).start();
    }
}
