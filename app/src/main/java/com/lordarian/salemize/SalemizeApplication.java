package com.lordarian.salemize;

import android.app.Application;

import com.lordarian.salemize.Helpers.UI.FontsOverride;

/**
 * Created by ebrah on 9/15/2017.
 */

public class SalemizeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/IRANSansMobile.ttf");
    }
}
